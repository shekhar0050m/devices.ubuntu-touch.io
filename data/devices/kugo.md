---
name: "Sony Xperia X Compact (F5321)"
deviceType: "phone"
maturity: .8

externalLinks:
  - name: "Device port post"
    link: "https://github.com/Halium/projectmanagement/issues/103"
    icon: "github"
---
